import cv2
import face_recognition
import numpy as np
import pytesseract

# Almacenando una 'codificación' universal de los rasgos faciales de la cedula de identificacion
Foto_Identificacion = face_recognition.load_image_file("Imagenes Cedulas\\cedula1.jpg")
Foto_Identificacion_Reconocida1 = face_recognition.face_encodings(Foto_Identificacion)[0]

# Extrayendo el texto de la cedula de identificacion ya previamente reconocida
imagen = cv2.imread("Imagenes Cedulas\\cedula1.jpg")
imagenreajustada = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
pytesseract.pytesseract.tesseract_cmd ='C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
texto = pytesseract.image_to_string(imagenreajustada)
texto_Limpio = [x.strip() for x in texto.split(',')]

#Creando un bucle para almacenar los caracteres del texto de la imagen ya reconocida o aprendida
Filas = []
Division_de_Cadena = texto_Limpio
for i in Division_de_Cadena:
    Filas.append(i)



# Almacenando una 'codificación' universal de los rasgos faciales de la cedula de identificacion a comparar
Foto_Identificacion2 = face_recognition.load_image_file("Imagenes Cedulas\\cedula2.jpg")
Foto_Identificacion_Reconocida2 = face_recognition.face_encodings(Foto_Identificacion2)[0]

# Extrayendo el texto de la cedula de identificacion a comparar
imagen2 = cv2.imread("Imagenes Cedulas\\cedula2.jpg")
imagenreajustada2 = cv2.cvtColor(imagen2, cv2.COLOR_BGR2GRAY)
pytesseract.pytesseract.tesseract_cmd ='C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
texto2 = pytesseract.image_to_string(imagenreajustada2)
texto_Limpio2 = [x.strip() for x in texto2.split(',')]

#Creando un bucle para almacenar los caracteres de la imagen que estan almacenados 
# en la variable texto2 y se va a comparar con la que esta reconocida.
Filas2 = []
Division_de_Cadena2 = texto_Limpio2
for i2 in Division_de_Cadena2:
    Filas2.append(i2)



#Creando dos bucles que verificaran si la mayoria de los datos coinciden entre las dos imagenes
#Tambien almacena los datos compatibles e incompatibles que detecte.
Datos_Compatibles = []
Datos_Incompatibles = []
Datos_Correctos = 0
Datos_Incorrectos = 0

for Dato in Filas:
    if (Dato in Filas2):
        Datos_Compatibles.append(Dato)
        Datos_Correctos = Datos_Correctos + 1

for Dato in Filas:
    if (Dato not in Filas2):
        Datos_Incompatibles.append(Dato)
        Datos_Incorrectos = Datos_Incorrectos + 1

if Datos_Correctos > Datos_Incorrectos:
    print("Los datos verificados son correctos.")
else:
    print("Los datos verificados son incorrectos.")


# Intruccion para detectar si las fotos de perfil en las dos cedulas de identificacion coinciden

Coincidencia = face_recognition.compare_faces([Foto_Identificacion_Reconocida1], Foto_Identificacion_Reconocida2)
if Coincidencia==[True] and Datos_Correctos > Datos_Incorrectos:
    print("La foto de identificacion verificada es correcta, por tanto su cedula es verdadera.")
else:
    print("La foto de identificacion verificada es Incorrecta, por tanto su cedula es falsa.")
